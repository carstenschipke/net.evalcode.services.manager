package net.evalcode.services.manager.management.jmx;


import net.evalcode.services.manager.annotation.Service;


/**
 * ServiceMBean
 *
 * @author carsten.schipke@gmail.com
 */
@Service
public interface ServiceMBean
{
  // ACCESSORS/MUTATORS
  String getName();
}
