package net.evalcode.services.manager.persistence;


/**
 * SchemaUtil
 *
 * @author carsten.schipke@gmail.com
 */
public final class SchemaUtil
{
  // PREDEFINED PROPERTIES
  public static final int DEFAULT_LENGTH_VARCHAR=255;


  // CONSTRUCTION
  private SchemaUtil()
  {
    super();
  }
}
